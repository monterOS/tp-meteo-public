<?php
namespace lib;

class Meteo
{

  private $appid;
  private $units;
  private $lang;
  private $url;

  public function __construct($appid, $units = "metric", $lang = "fr") {
    $this->appid = $appid;
    $this->units = $units;
    $this->lang = $lang;
    $this->url = "http://api.openweathermap.org/data/2.5/weather?q={{ville}}&units=metric&lang=fr&appid=$this->appid";
  }

  public function getDataByCity(string $city): array {
    $url = preg_replace('/{{ville}}/', $city, $this->url);
    $content = json_decode(file_get_contents($url));

    if (empty($content->wind->deg)) {
      $content->wind->deg = "N/A";
    }

    $data = [
      "city" => $content->name,
      "temperature" => $content->main->temp,
      "humidity" => $content->main->humidity,
      "wind" => [
        "speed" => $content->wind->speed,
        "deg" => $content->wind->deg
      ],
      "weather" => $content->weather[0]->main,
      "description" => $content->weather[0]->description
    ];

    return $data;
  }

  public function getTemperatureByCity(string $city) {
    $url = preg_replace('/{{ville}}/', $city, $this->url);
    $content = json_decode(file_get_contents($url));

    $temperature = $content->main->temp;
    return $temperature;
  }

}
