<?php
namespace lib;

class Validation
{
  protected $errors = [];
  protected $data;
  protected $files;

  /**
   * Initialisation des variables protected
   * @param  array  $data $_POST
   * @param  array  $files $_FILES
   */
  public function initValidation(array $data, array $files) {
    $this->errors = [];
    $this->data = $data;
    $this->files = $files;
  }

  /**
   * Data Getter
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Data Setter
   * @param string $content
   */
  public function setData(string $content) {
    $this->data = $content;
  }

  /**
   * Files Getter
   */
  public function getFiles() {
    return $this->files;
  }

  /**
   * Files Setter
   * @param string $content
   */
  public function setFiles(string $content) {
    $this->files = $content;
  }

  /**
   * Création d'un tableau avec les données a envoyer en base de donnée et upload des fichiers
   * @return array  [ville, temperature, fichier_local, fichier_distant]
   */
  public function getValidateData() {
    $validateData = [
      "ville" => $this->data['ville'],
      "fichier_local" => 'C:\wamp64\www\fab-meteo\images/'.$this->files['fichier_local']['name'],
      "fichier_distant" => $this->data['fichier_distant']
    ];

    $this->uploadFile('fichier_local');

    return $validateData;
  }

  /**
   * Début des validations pour chaque $champs passer en parametres.
   * Si pas d'erreur, on appel la methode, sinon erreur.
   */
  public function validation(string $champs, string $methode, ...$parametres) {
    if (empty($this->data[$champs]) || !isset($this->data[$champs]))
    {
      $this->errors[$champs] = "Le champs $champs n'est pas rempli";
    }
    else
    {
      call_user_func([$this, $methode], $champs, ...$parametres);
    }
  }

  /**
   * Validation pour les $_FILES
   * Si pas d'erreur, on appel la methode, sinon erreur.
   */
  public function validationFile(string $champs, string $methode, ...$parametres) {
    if (!isset($this->files[$champs]))
    {
      $this->errors[$champs] = "Vous n'avez pas inséré d'image.";
    }
    else
    {
      call_user_func([$this, $methode], $champs, ...$parametres);
    }
  }

  /**
   * Upload des images dans le dossier images.
   */
  public function uploadFile(string $champs) {
    if (isset($_FILES[$champs]))
    {
      $fichier = $_FILES[$champs]['tmp_name'];
      if (preg_match('/image/',$_FILES[$champs]['type']))
      {
        $destination = $_FILES[$champs]['name'];
        move_uploaded_file($fichier, 'C:\wamp64\www\fab-meteo\images/'.$destination);
      }
    }
  }

  /**
   * @param string $champs
   * Si le champs n'est pas de type alpha on renvoie une erreur
   */
  public function isAlpha(string $champs) {
    if (!ctype_alpha($this->data[$champs]))
    {
      $this->errors[$champs] = "Le champs n'est pas de type Alpha-Numérique";
    }
  }

  /**
   * @param string $champs
   * Si le champs n'est pas de type digit on renvoie une erreur
   */
  public function isDigit(string $champs) {
    if (!ctype_digit($this->data[$champs]))
    {
      $this->errors[$champs] = "Le champs n'est pas de type digitale";
    }
  }

  /**
   * @param string $champs
   * Si le champs n'est pas de type URL on renvoie une erreur
   */
  public function isURL(string $champs) {
    if(!filter_var($this->data[$champs], FILTER_VALIDATE_URL))
    {
      $this->errors[$champs] = "Vous n'avez pas rentrer une URL valide";
    }
  }

  /**
   * @param string $champs
   * Methode afin de faire toute les vérifications des conditions
   */
  public function validateImage(string $champs) {
    $this->isImage($champs);
    $this->errorFile($champs);
  }

  /**
   * @param string $champs
   * Si l'images n'est pas de type image on renvoie une erreur
   */
  public function isImage(string $champs) {
    if (!preg_match('/image/',$_FILES[$champs]['type']))
    {
      $this->errors[$champs] = "Votre fichier n'est pas une image";
    }
  }

  /**
   * @param string $champs
   * Si error est différent de 0 on renvoie une erreur
   */
  public function errorFile(string $champs) {
    if ($this->files[$champs]['error'] != 0)
    {
      $this->errors[$champs] = "L'upload de votre image n'a pas fonctionner, veuillez recommencer.";
    }
  }

}
