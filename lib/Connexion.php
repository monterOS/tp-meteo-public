<?php


namespace lib;

use \PDO;

class Connexion
{
  private $host;
  private $dbName;
  private $user;
  private $password;
  private $bdd;

  public function __construct($dbName, $host = "localhost", $user = "root", $password = "")
  {
    $this->host = $host;
    $this->dbName = $dbName;
    $this->$user = $user;
    $this->password = $password;

    $this->bdd = new PDO('mysql:host='.$this->host.'; dbname='.$this->dbName.'', ''.$this->$user.'', ''.$this->password.'');
  }

  public function getBDD() {
    return $this->bdd;
  }

  public function requete($requete) {
    $data = $this->getBDD()->query($requete);
    $data = $data->fetchAll(PDO::FETCH_ASSOC);
    return $data;
  }

}
