<?php
namespace lib;

class Formulaire
{
  private $encapsulation;

  public function __construct() {
    $this->encapsulation = 'div';
  }

  public function encapsulation($contenu) {
    return "<{$this->encapsulation} class='form-group'>$contenu</{$this->encapsulation}>";
  }

  public function input(array $parametres) {
    extract($parametres);

    if(isset($label)) {
      return $this->encapsulation("
        <label>$label</label>
        <input class='form-control' type='$type' name='$name' value='$value' placeholder='$placeholder'/>
      ");
    }
    return $this->encapsulation("<input class='form-control' type='$type' name='$name' value='$value' placeholder='$placeholder'/>");
  }

  public function submit($message) {
    return "<button type='submit' class='btn btn-primary'>$message</button>";
  }

}
