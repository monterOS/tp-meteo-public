<?php
namespace lib;

class RelevesValidation extends Validation
{

  public function initValidation(array $data, array $files) {
    parent::initValidation($data, $files);
    $this->validation('ville', 'isAlpha');
    $this->validation('fichier_distant', 'isURL');
    if (!empty($this->errors['fichier_distant'])) {
      $this->validationFile('fichier_local', 'validateImage');
    }
    return $this->errors;
  }

  /**
   * Data Getter
   */
  public function getData() {
    return parent::getData();
  }

  /**
   * Data Setter
   * @param string $content
   */
  public function setData(string $content) {
    return parent::setData($content);
  }

  /**
   * Files Getter
   */
  public function getFiles() {
    return parent::getFiles();
  }

  /**
   * Files Setter
   * @param string $content
   */
  public function setFiles(string $content) {
    return parent::setFiles($content);
  }
}
