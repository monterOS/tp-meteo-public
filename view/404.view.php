<?php http_response_code(404) ?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Météo - Saisie</title>
    <link rel="stylesheet" href="vendor/bootstrap-4/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <h1 class="text-center">Weather is not found</h1>
      <a href="/" class="btn btn-primary">Accueil</a>
    </div>
    <script src="vendor/jquery-3.3.1/js/jquery.js" charset="utf-8"></script>
    <script src="vendor/popper-1.12.9/js/popper.js" charset="utf-8"></script>
    <script src="vendor/bootstrap-4/js/bootstrap.min.js" charset="utf-8"></script>
  </body>
</html>
