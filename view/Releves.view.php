<?php
use lib\Connexion;
use lib\Meteo;

$weatherData = new Meteo("a6b6a0fa5ad67e3666cb91e4c3a8f2f4");
$pdo = new Connexion('fab');
$data = $pdo->requete('SELECT * FROM releves WHERE city = "'.$decompositionUri[1].'"');
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Météo - Saisie</title>
    <link rel="stylesheet" href="vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/master.css">
  </head>
  <body class="bg-light">

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="/"><img src="../logo.png" alt="Logo de la Météo Montero" width="80"> Météo Montero</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/">Accueil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/saisie">Saisie</a>
          </li>
        </ul>
      </div>
    </nav>


    <div class="container">

      <!-- Bandeau Météo Montero -->
      <div class="d-flex align-items-center p-3 my-3 text-light bg-primary rounded box-shadow">
        <div class="row">
          <div class="col">
            Ville
          </div>
        </div>
      </div>

      <!-- ... -->
      <?php

      // En développement

        $dataArray = [];

        foreach($data as $key => $value) {
          $date = explode(' ', $value['date_report'])[0];

          if (!isset($dataArray[$date])) {
            $dataArray[$date] = [$value];
          } else {
            $dataArray[$date][] = $value;
          }

        }

        echo "<pre>";
        print_r($dataArray);
        echo "</pre>";

      ?>

      <!-- Pied de page -->




    </div>
    <script src="vendor/jquery-3.3.1/js/jquery.js" charset="utf-8"></script>
    <script src="vendor/popper-1.12.9/js/popper.js" charset="utf-8"></script>
    <script src="vendor/bootstrap-4/js/bootstrap.min.js" charset="utf-8"></script>
  </body>
</html>
