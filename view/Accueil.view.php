<?php
use lib\Connexion;
use lib\Meteo;

$weatherData = new Meteo("a6b6a0fa5ad67e3666cb91e4c3a8f2f4");
$pdo = new Connexion('fab');
$data = $pdo->requete('SELECT * FROM ville');
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Météo - Saisie</title>
    <link rel="stylesheet" href="vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/master.css">
    <link rel="stylesheet" href="../css/sticky-footer.css">
  </head>
  <body class="bg-light">

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark box-shadow">
      <a class="navbar-brand" href="/"><img src="../logo.png" alt="Logo de la Météo Montero" width="80"> Météo Montero</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/">Accueil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/saisie">Saisie</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">

      <!-- Bandeau Météo Montero -->
      <div class="d-flex align-items-center p-3 my-3 text-light bg-primary rounded box-shadow">
      </div>

      <!-- Filtre des relevés selon les régles -->
      <ul class="nav justify-content-center">
        <li class="nav-item">
          <a class="nav-link" href="#">RAZ</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Villes <15°</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Villes entre 16 et 20°</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Villes entre 21 et 25°</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Villes > 25°</a>
        </li>
      </ul>
    </div> <!-- /container -->

    <div class="container-fluid mb-5">
      <!-- Affichage des relevés de l'utilisateur -->
      <div class="row">
        <?php foreach($data as $key => $value): ?>
        <div class="col-4">
          <div class="card box-shadow mt-5" style="width:25rem">
            <img class="card-img-top" src="<?= $value['photo'] ?>" alt="Photo de la ville de <?= $value['ville'] ?>">
            <div class="card-body">
              <h5 class="card-title"><?= $value['ville'] ?></h5>
              <p class="card-text">Il fait <?= $weatherData->getTemperatureByCity($value['ville']) ?>°C à <?= $value['ville'] ?></p>
              <a href="/releves=<?= $value['ville'] ?>" class="btn btn-primary">Voir les anciens relevés</a>
            </div>
          </div>
        </div>
        <?php endforeach; ?>
      </div>
    </div>

    <!-- Pied de page -->
    <footer class="footer bg-dark">
      <div class="container text-center">
        Développé par Lorca Montero
      </div>
    </footer>

    <script src="vendor/jquery-3.3.1/js/jquery.js" charset="utf-8"></script>
    <script src="vendor/popper-1.12.9/js/popper.js" charset="utf-8"></script>
    <script src="vendor/bootstrap-4/js/bootstrap.min.js" charset="utf-8"></script>
  </body>
</html>
