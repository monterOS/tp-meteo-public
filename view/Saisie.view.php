<?php

use lib\Formulaire;
use lib\Validation;
use lib\RelevesValidation;

$form = new Formulaire();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $errors = [];
  $validation = new RelevesValidation();
  $errors = $validation->initValidation($_POST, $_FILES);
  if (empty($errors)) {
    $data = $validation->getValidateData();
    $req = $bdd->prepare('INSERT INTO ville (ville, photo) VALUES (?,?)');
    $req->execute([$data['ville'], $data['fichier_distant'].'|'.$data['fichier_local']]);
  }
  else {
    $sauvegardeData = $validation->getData();
  }
}

?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Météo - Saisie</title>
    <link rel="stylesheet" href="vendor/bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/master.css">
  </head>
  <body class="bg-light">

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="/"><img src="../logo.png" alt="Logo de la Météo Montero" width="80"> Météo Montero</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/">Accueil</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/saisie">Saisie</a>
          </li>
        </ul>
      </div>
    </nav>

    <!-- Formulaire -->
    <div class="container">
      <?php if (!empty($errors)): ?>
        <div class="alert alert-danger" role="alert">
          Attention, vous avez plusieurs erreurs, merci de les corriger !
        </div>
      <?php endif; ?>

      <!-- Bandeau -->
      <div class="d-flex align-items-center p-3 my-3 text-light bg-primary rounded box-shadow">
        <h1>Ajouter une ville</h1>
      </div>

      <form action="" method="post" enctype="multipart/form-data">
        <?= $form->input(["type" => "text", "name" => "ville", "value" => (empty($errors)) ? '' : $sauvegardeData['ville'], "placeholder" => "Indiquez une ville", "label" => "Nom de la ville :"]) ?>
        <?php if (isset($errors['ville'])): ?>
          <p class="text-danger"><?= $errors['ville'] ?></p>
        <?php endif; ?>

        <?= $form->input(["type" => "file", "name" => "fichier_local", "value" => "", "placeholder" => "", "label" => "Photo de la ville :"]) ?>
        <?php if (isset($errors['fichier_local'])): ?>
          <p class="text-danger"><?= $errors['fichier_local'] ?></p>
        <?php endif; ?>

        <?= $form->input(["type" => "text", "name" => "fichier_distant", "value" => (empty($errors)) ? '' : $sauvegardeData['fichier_distant'], "placeholder" => "", "label" => "Photo de la ville par url :"]) ?>
        <?php if (isset($errors['fichier_distant'])): ?>
          <p class="text-danger"><?= $errors['fichier_distant'] ?></p>
        <?php endif; ?>

        <?= $form->submit('Valider') ?>
      </form>
    </div>

    <script src="vendor/jquery-3.3.1/js/jquery.js" charset="utf-8"></script>
    <script src="vendor/popper-1.12.9/js/popper.js" charset="utf-8"></script>
    <script src="vendor/bootstrap-4/js/bootstrap.min.js" charset="utf-8"></script>
  </body>
</html>
