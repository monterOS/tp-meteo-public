<?php

require 'vendor/autoload.php';

CONST PHP_CLASSPATH=__DIR__.'/';
spl_autoload_register(function($class){
  $file=PHP_CLASSPATH.str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';
  if(file_exists($file))
  {
    require $file;
  }
});
