<?php

require_once "bootstrap.php";

use lib\Meteo;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use lib\Connexion;

$uri = $_SERVER['REQUEST_URI'];
$decompositionUri = explode('=', $uri);

/**
 * Routeur maison
 * A créer : Controller pour séparer le code du HTML
 */
switch ($uri) {
  case '/':
    require 'view/Accueil.view.php';
    break;

  case '/saisie':
    require 'view/Saisie.view.php';
    break;

  case "/releves=$decompositionUri[1]":
    require 'view/Releves.view.php';
    break;

  default:
    require 'view/404.view.php';
    break;
}
